FROM php:7.4-fpm

######
# You can configure php extensions using docker-php-ext-configure
# You can install php extensions using docker-php-ext-install
######

# define timezone
#RUN echo "America/Sao_Paulo" > /etc/timezone
#RUN dpkg-reconfigure -f noninteractive tzdata
#RUN /bin/echo -e "LANG=\"en_US.UTF-8\"" > /etc/default/local

RUN export TZ="America/Fortaleza"

# install dependencies
RUN apt-get update
RUN apt-get install -y --no-install-recommends \
    build-essential \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libmcrypt-dev \
    libpng-dev \
    libwebp-dev \
    curl \
    libcurl4 \
    libcurl4-openssl-dev \
    zlib1g-dev \
    libicu-dev \
    libmemcached-dev \
    memcached \
    default-mysql-client \
    libmagickwand-dev \
    unzip \
    libzip-dev \
    zip \
    nano;

# memcached
RUN pecl install memcached-3.1.5
RUN docker-php-ext-enable memcached

# mcrypt
RUN pecl install mcrypt-1.0.3
RUN docker-php-ext-enable mcrypt

# configure, install and enable all php packages
RUN docker-php-ext-configure gd --enable-gd --with-freetype --with-jpeg --with-webp
RUN cd /usr/src/php/ext/gd && make
RUN cp /usr/src/php/ext/gd/modules/gd.so /usr/local/lib/php/extensions/no-debug-non-zts-20190902/gd.so
RUN docker-php-ext-install -j$(nproc) gd

RUN docker-php-ext-configure pdo_mysql --with-pdo-mysql=mysqlnd
RUN docker-php-ext-configure mysqli --with-mysqli=mysqlnd
RUN docker-php-ext-configure intl
RUN docker-php-ext-configure zip

RUN docker-php-ext-install -j$(nproc) opcache
RUN docker-php-ext-install -j$(nproc) pdo_mysql
RUN docker-php-ext-install -j$(nproc) mysqli
RUN docker-php-ext-install -j$(nproc) pdo
RUN docker-php-ext-install -j$(nproc) gd
RUN docker-php-ext-install -j$(nproc) intl
RUN docker-php-ext-install -j$(nproc) zip

# install predis (extension run redis)
RUN pecl install -o -f redis \ &&  rm -rf /tmp/pear \ &&  docker-php-ext-enable redis

# install xdebug
RUN pecl install xdebug
RUN docker-php-ext-enable xdebug

RUN echo "xdebug.remote_enable=1" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "xdebug.remote_autostart=0" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "xdebug.default_enable=0" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "xdebug.remote_host=host.docker.internal" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "xdebug.remote_port=9000" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "xdebug.remote_connect_back=0" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "xdebug.profiler_enable=0" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN echo "xdebug.remote_log=\"/tmp/xdebug.log\"" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

# configure opcache
RUN echo "opcache.memory_consumption=128" >> /usr/local/etc/php/conf.d/opcache-recommended.ini
RUN echo "opcache.interned_strings_buffer=8" >> /usr/local/etc/php/conf.d/opcache-recommended.ini
RUN echo "opcache.max_accelerated_files=4000" >> /usr/local/etc/php/conf.d/opcache-recommended.ini
RUN echo "opcache.revalidate_freq=2" >> /usr/local/etc/php/conf.d/opcache-recommended.ini
RUN echo "opcache.fast_shutdown=1" >> /usr/local/etc/php/conf.d/opcache-recommended.ini

# install imagick
RUN pecl install imagick-3.4.4
RUN docker-php-ext-enable imagick

#install nodejs 14 lts
RUN cd /tmp \
    && curl -sL https://deb.nodesource.com/setup_lts.x | bash - \
    && apt-get install -y nodejs

# install composer
RUN cd /tmp \
    && curl -sS https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer

#install nodejs
#RUN

# clean image
RUN apt-get clean


#FROM php:7.4-alpine3.12
## Install system packages & PHP extensions required for Laravel
#RUN apk --update add \
#        git \
#        curl \
#        curl-dev \
#        bash \
#        bash-completion \
#        freetype-dev \
#        icu \
#        icu-dev \
#        libxml2-dev \
#        libintl \
#        libjpeg-turbo-dev \
#        libpng-dev \
#        mysql-client \
#        nodejs \
#        postgresql-dev && \
#    docker-php-ext-configure gd \
#        --with-gd \
#        --with-freetype-dir=/usr/include/ \
#        --with-jpeg-dir=/usr/include/ \
#        --with-png-dir=/usr/include/ && \
#    docker-php-ext-configure bcmath && \
#    docker-php-ext-configure pgsql --with-pgsql=/usr/local/pgsql && \
#    docker-php-ext-install \
#        soap \
#        zip \
#        curl \
#        bcmath \
#        exif \
#        gd \
#        iconv \
#        intl \
#        mbstring \
#        opcache \
#        pdo_mysql \
#        pdo_pgsql \
#        mysqli \
#        pdo \
#        pcntl \
#        tokenizer \
#        xml \
#        pgsql && \
#    apk del \
#        icu-dev \
#        gcc \
#        g++ && \
#    apk add --no-cache tzdata && \
#    set -ex && \
## memcache
#    apk add --no-cache --virtual .memcached-deps zlib-dev libmemcached-dev cyrus-sasl-dev && \
#    docker-php-source extract && \
#    git clone --branch php7 https://github.com/php-memcached-dev/php-memcached /usr/src/php/ext/memcached/ && \
#    docker-php-ext-install memcached && \
##    docker-php-source delete && \
#    apk add --no-cache --virtual .memcached-runtime-deps libmemcached-libs && \
#    apk del .memcached-deps && \
## imagick
#    apk add --no-cache --virtual .phpize-deps $PHPIZE_DEPS imagemagick-dev libtool && \
#    export CFLAGS="$PHP_CFLAGS" CPPFLAGS="$PHP_CPPFLAGS" LDFLAGS="$PHP_LDFLAGS" && \
#    pecl install imagick-3.4.3 && \
#    docker-php-ext-enable imagick && \
#    apk add --no-cache --virtual .imagick-runtime-deps imagemagick && \
##    apk del .phpize-deps && \
## apcu
##    apk add --no-cache --virtual .phpize-deps $PHPIZE_DEPS && \
##    docker-php-source extract && \
#    pecl install apcu && \
#    docker-php-ext-enable apcu && \
#    docker-php-source delete && \
#    apk del .phpize-deps && \
#    rm -r /tmp/pear/* && \
#    echo -e "expose_php = off\n\
#apc.enable_cli = 1\n\
#cgi.fix_pathinfo = 0" >> /usr/local/etc/php/php.ini
##
### Configure version constraints
##ENV VERSION_PRESTISSIMO_PLUGIN=^0.3.0 \
##    COMPOSER_ALLOW_SUPERUSER=1
##
##RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer && \
##    composer global require --optimize-autoloader \
##        "hirak/prestissimo:${VERSION_PRESTISSIMO_PLUGIN}" && \
##    composer global dumpautoload --optimize
##
### xdebug
##ENV XDEBUG_VERSION 2.7.0alpha1
##RUN apk --no-cache add --virtual .build-deps \
##        g++ \
##        autoconf \
##        make && \
##    pecl install xdebug-$XDEBUG_VERSION && \
##    docker-php-ext-enable xdebug && \
##    apk del .build-deps && \
##    rm -r /tmp/pear/* && \
##    echo -e "xdebug.remote_port = 9001\n\
##xdebug.idekey = \"PHPSTORM\"\n\
##xdebug.remote_enable=on" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
