<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ManutencaoProgramada
 *
 * @method static \Illuminate\Database\Eloquent\Builder|ManutencaoProgramada newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ManutencaoProgramada newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ManutencaoProgramada query()
 * @mixin \Eloquent
 */
class ManutencaoProgramada extends Model
{
    use HasFactory;
}
