<?php

use MasterTag\DataHora;

require __DIR__ . '/vendor/autoload.php';
$app = require_once __DIR__ . '/bootstrap/app.php';
$kernel = $app->make(Illuminate\Contracts\Http\Kernel::class);

$response = $kernel->handle(
    $request = Illuminate\Http\Request::capture()
);

$kernel->terminate($request, $response);

ini_set("memory_limit", -1);
ini_set('max_execution_time', -1);

echo "iniciando a migração \n";
/*
//Parecer RH
$parecerrhs = \App\Models\ParecerRh::get();
foreach ($parecerrhs as $linha) {
    $feedback = \App\Models\FeedbackCurriculo::whereCurriculoId($linha->curriculo_id)->select('id')->first();
    $linha->update(['feedback_id' => $feedback->id]);
}

echo "Parecer Rh - ok \n";

//parecer_rotas
$parecer_rotas = \App\Models\ParecerRota::get();
foreach ($parecer_rotas as $item) {
    $feedback = \App\Models\FeedbackCurriculo::whereCurriculoId($item->curriculo_id)->select('id')->first();
    $item->update(['feedback_id' => $feedback->id]);
}
echo "Parecer Rotas - ok \n";

//parecer_teste_pratico
$parecer_teste_pratico = \App\Models\ParecerTestePratico::get();
foreach ($parecer_teste_pratico as $item) {
    $feedback = \App\Models\FeedbackCurriculo::whereCurriculoId($item->curriculo_id)->select('id')->first();
    $item->update(['feedback_id' => $feedback->id]);
}
echo "Parecer Teste Pratico - ok \n";

//parecer_entrevista_tecnica
$parecer_teste_pratico = \App\Models\ParecerEntrevistaTecnica::get();
foreach ($parecer_teste_pratico as $item) {
    $feedback = \App\Models\FeedbackCurriculo::whereCurriculoId($item->curriculo_id)->select('id')->first();
    $item->update(['feedback_id' => $feedback->id]);
}

echo "Parecer Entrevista Tecnica - ok \n";
//resultado_integrados
$resultado_integrados = \App\Models\ResultadoIntegrado::get();
foreach ($resultado_integrados as $item) {
    $feedback = \App\Models\FeedbackCurriculo::whereCurriculoId($item->curriculo_id)->select('id')->first();
    $item->update(['feedback_id' => $feedback->id]);
}

echo "Resultado Integrado - ok \n";

//individual_rhs
$individual_rhs = \App\Models\IndividualRh::get();
foreach ($individual_rhs as $item) {
    $feedback = \App\Models\FeedbackCurriculo::whereCurriculoId($item->curriculo_id)->select('id')->first();
    $item->update(['feedback_id' => $feedback->id]);
}

echo "Individual RH - ok \n";

//gestor_rhs
$gestor_rhs = \App\Models\GestorRh::get();
foreach ($gestor_rhs as $item) {
    $feedback = \App\Models\FeedbackCurriculo::whereCurriculoId($item->curriculo_id)->select('id')->first();
    $item->update(['feedback_id' => $feedback->id]);
}

echo "Gestor RH - ok \n";

//admissoes
$admissoes = \App\Models\Admissao::get();
foreach ($admissoes as $item) {
    $feedback = \App\Models\FeedbackCurriculo::whereCurriculoId($item->curriculo_id)->select('id')->first();
    $item->update(['feedback_id' => $feedback->id]);
}

echo "Admissoes - ok \n";

//certificado_alumar
$certificado_alumar = \App\Models\CertificadoAlumar::get();
foreach ($certificado_alumar as $item) {
    $feedback = \App\Models\FeedbackCurriculo::whereCurriculoId($item->curriculo_id)->select('id')->first();
    $item->update(['feedback_id' => $feedback->id]);
}

echo "Certificado Alumar- ok \n";

//entrevista_desligamentos
$entrevista_desligamentos = \App\Models\EntrevistaDesligamento::get();
foreach ($entrevista_desligamentos as $item) {
    $feedback = \App\Models\FeedbackCurriculo::whereCurriculoId($item->curriculo_id)->select('id')->first();
    $item->update(['feedback_id' => $feedback->id]);
}

echo "Desligamento - ok \n";

//treinamentos
$treinamentos = \App\Models\Treinamento::get();
foreach ($treinamentos as $item) {
    $feedback = \App\Models\FeedbackCurriculo::whereCurriculoId($item->curriculo_id)->select('id')->first();
    $item->update(['feedback_id' => $feedback->id]);
}

echo "Treinamentos - ok \n";

//vinculos
$vinculos = \App\Models\Vinculo::get();
foreach ($vinculos as $item) {
    $feedback = \App\Models\FeedbackCurriculo::whereCurriculoId($item->curriculo_id)->select('id')->first();
    $item->update(['feedback_id' => $feedback->id]);
}

echo "Vinculos - ok \n";


//notificacao_whats
$notificacao_whats = \App\Models\Admissao::get();
foreach ($notificacao_whats as $item) {
    $feedback = \App\Models\FeedbackCurriculo::whereCurriculoId($item->curriculo_id)->select('id')->first();
    $item->update(['feedback_id' => $feedback->id]);
}
echo "Notificacao WhatsApp - ok \n";


$etapas = \App\Models\Etapas::get();
foreach ($etapas as $item) {
    $feedback = \App\Models\FeedbackCurriculo::whereCurriculoId($item->curriculo_id)->select('id')->first();
    $item->update(['feedback_id' => $feedback->id]);
}
echo "Etapas - ok \n";


$simuladoCandidatos = \App\Models\SimuladoCandidato::get();
foreach ($simuladoCandidatos as $item) {
    $feedback = \App\Models\FeedbackCurriculo::whereCurriculoId($item->curriculo_id)->select('id')->first();
    $item->update(['feedback_id' => $feedback->id]);
}
echo "Simulado Candidatos - ok \n";
*/
$simuladoCandidatosResposta = \App\Models\SimuladoCandidatoResposta::get();
foreach ($simuladoCandidatosResposta as $item) {
    $feedback = \App\Models\FeedbackCurriculo::whereCurriculoId($item->curriculo_id)->select('id')->first();
    $item->update(['feedback_id' => $feedback->id]);
}
echo "Simulado Candidatos Resposta - ok \n";


//foreach ($areaNormal as $area){
//    $areas = DB::connection('mysql')->table('areas')->insert([
//        'label' => $area->label,
//        'ativo' => $area->ativo
//    ]);
//}
//
