@extends('layouts.sistema')
@section('title', 'CONTROLE DE EXAMES')
@section('content_header', 'CONTROLE DE EXAMES')
@section('content')

    <modal id="janelaParecerEntrevista" :titulo="tituloJanela" :size="80" :fechar="!preload">
        <template slot="conteudo">
            <preload v-if="preload"></preload>
            <div v-if="!preload && (!cadastrado && !atualizado) && form.id !== ''">
                <fieldset>
                    <legend class="text-uppercase">Dados Pessoais</legend>
                    <div class="row">
                        <div class="col-12">
                            <p>
                                Nome: <strong>@{{ dados.nome }}</strong> <br>
                                <br>
                                Cargo: <strong>@{{ dados.cargo }}</strong> <br>
                            </p>
                        </div>
                    </div>
                </fieldset>

                <ul class="nav nav-tabs bg-light" id="tabslist" role="tablist"
                    style="border-bottom: 1px solid #653232">
                    <li class="nav-item">
                        <a class="nav-item nav-link active" id="nav-encaminhar-tab" data-toggle="tab"
                           href="#nav-encaminhar"
                           role="tab" aria-controls="nav-encaminhar" aria-selected="false">ENCAMINHAR</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-item nav-link" id="nav-encaminhados-tab" data-toggle="tab"
                           href="#nav-encaminhados"
                           role="tab" aria-controls="nav-encaminhados" aria-selected="true">ENCAMINHADOS</a>
                    </li>

                </ul>

                <div class="tab-content py-3 p-2">
                    <div class="tab-pane fade show active" id="nav-encaminhar" role="tabpanel"
                         aria-labelledby="nav-encaminhar-tab">
                        <fieldset>
                            <legend class="text-uppercase">Clinica</legend>
                            <div class='row'>
                                <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <select class='form-control'
                                                onblur='valida_campo_vazio(this,1)'
                                                onchange='valida_campo_vazio(this,1)'
                                                v-model='form.empresa_exame_id'
                                        >
                                            <option value=''>Selecione</option>
                                            <option v-for='item in listaEmpresasExames' :value='item.id'>@{{ item.nome
                                                }}
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                                    <div class="form-group">
                                        <h5 v-if='form.empresa_exame_id' class='my-2'
                                              v-text="listaEmpresasExames.filter(item => item.id === form.empresa_exame_id)[0].dados.email"></h5>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <formulario :model='form' :formulario_id='2' v-if='form.formulario'
                                    :mostra_titulo='false'></formulario>

                    </div>

                    <div class="tab-pane fade" id="nav-encaminhados" role="tabpanel"
                         aria-labelledby="nav-encaminhados-tab">

                        <div class="alert alert-warning text-center" v-show="!preload && historico.length===0">
                            <i class="fa fa-exclamation-triangle"></i> Nenhum Encaminhamento Encontrado.
                        </div>

                        <table class="tabela table-striped" v-show="!preload && historico.length > 0">
                            <thead>
                            <tr class="bg-default">
                                <th>CÓD</th>
                                <th>Tipo de exame</th>
                                <th>Clinica</th>
                                <th>Encaminhado Por</th>
                                <th>Data do Encaminhamento</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody v-for="item in historico">
                            <tr style="background: white !important; border-bottom: none">
                                <td>@{{ item.id }}</td>
                                <td>@{{ item.tipo_exame }}</td>
                                <td>@{{ item.empresa_exame.nome }}</td>
                                <td>@{{ item.quem_encaminhou.nome }}</td>
                                <td>@{{ item.created_at }}</td>
                                <td>
                                    <form :action="`${URL_ADMIN}/controle-exames/ficha-encaminhamento/${item.id}`"
                                          target="_blank" method="post">
                                        @csrf
                                        <input type="hidden" name="id" :value="item.id">
                                        <input type="hidden" name="tipo_exame" :value="item.tipo_exame">
                                        <button type="submit" content="Gerar PDF" v-tippy
                                                class="btn btn-sm btn-primary mb-2">
                                            <i class="fa fa-file-pdf" aria-hidden="true"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </template>
        <template slot="rodape">
            <div v-show="!visualizar">
                <button type="button" class="btn btn-sm btn-primary" v-show="!cadastrado  && !preload"
                        @click.prevent="salvarUpdate">
                    <i class="fa fa-save"></i>
                    <span v-show='cadastrando'>Salvar</span>
                    <span v-show='editando'>Editando</span>
                </button>
            </div>
        </template>
    </modal>

    <modal id="filtroColunas" titulo="Mostrar e Ocultar colunas">
        <template slot="conteudo">
            <div class="custom-control custom-switch mb-2" v-if="AUTENTICADO.cliente_id === 0">
                <input type="checkbox" v-model="colunasTabela.cliente"
                       @click="colunasTabela.cliente = !colunasTabela.cliente" class="custom-control-input"
                       id="cliente">
                <label class="custom-control-label" for="cliente">EMPRESA</label>
            </div>
        </template>
    </modal>


    <fieldset>
        <legend>Filtro</legend>
        <form @submit.prevent="$refs.componente.buscar()">
            <div class="row">
                <div class="col-12 col-md-3">
                    <div class="form-check" style="margin-bottom: -11px;">
                        <input type="checkbox" class="form-check-input" :disabled="controle.carregando"
                               id="filtroIntervalo"
                               v-model="controle.dados.filtroPeriodo">
                        <label class="form-check-label cursor-pointer" for="filtroIntervalo">Por período</label>
                    </div>
                    <div class="form-group">
                        <datepicker range formsm label=""
                                    :disabled="controle.carregando || !controle.dados.filtroPeriodo"
                                    v-model="controle.dados.periodo"></datepicker>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="form-group">
                        <label>Nome</label>
                        <input type="text" placeholder="Buscar por nome" autocomplete="off"
                               class="form-control form-control-sm" :disabled="controle.carregando"
                               v-model="controle.dados.campoBusca">
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="form-group">
                        <label>CPF</label>
                        <input type="text" placeholder="Buscar por cpf" autocomplete="mastertag"
                               onblur="valida_cpf(this)"
                               v-mascara:cpf class="form-control form-control-sm" :disabled="controle.carregando"
                               v-model="controle.dados.campoCPF">
                    </div>
                </div>

                <div class="col-12 col-sm-4 col-md-3 col-lg-2">
                    <div class="form-group">
                        <label for="">Exibir</label>
                        <select class="form-control form-control-sm" @change="atualizar" :disabled="controle.carregando"
                                v-model="controle.dados.pages">
                            <option v-for='exib in exibicao' :value="exib">@{{exib}}</option>
                        </select>
                    </div>
                </div>
            </div>
        </form>
        <div class="col-12">
            <div class="row">
                <button type="button" class="btn btn-sm btn-success mr-1 mb-1" :disabled="controle.carregando"
                        @click.prevent="atualizar">
                    <i :class="controle.carregando ? 'fa fa-sync fa-spin' : 'fa fa-sync'"></i>
                    Atualizar
                </button>

                {{--                <button class="btn btn-sm btn-danger mb-1 mr-1"--}}
                {{--                        :style="selecionados.length === 0 ? 'cursor: not-allowed' : 'cursor: pointer'"--}}
                {{--                        :disabled="selecionados.length === 0" @click.prevent="selecionados = []">--}}
                {{--                    <i class="fa fa-times"></i> Limpar seleção--}}
                {{--                </button>--}}

            </div>
        </div>

    </fieldset>
    <preload class="text-center" v-if="controle.carregando"></preload>
    <div class="alert alert-warning text-center" v-show="!controle.carregando && lista.length===0">
        <i class="fa fa-exclamation-triangle"></i> Nenhum Registro Encontrado
    </div>

    <div id="conteudo">
        <table class="tabela table-striped" v-show="!controle.carregando && lista.length > 0">
            <thead>
            <tr class="bg-default">
                <th>ID</th>
                <th>Nome</th>
                <th>
                    {{--                    <button class="btn btn-sm btn-primary mb-2" content="Mostrar e Ocultar Colunas" v-tippy--}}
                    {{--                            data-toggle="modal"--}}
                    {{--                            data-target="#filtroColunas">--}}
                    {{--                        <i class="bx bxs-filter-alt" aria-hidden="true"></i>--}}
                    {{--                    </button>--}}
                </th>
            </tr>
            </thead>
            <tbody v-for="colaborador in lista">
            <tr style="background: white !important; border-bottom: none">
                <td>@{{ colaborador.id }}</td>
                <td>@{{ colaborador.curriculo.nome }}</td>
                <td class="text-center">
                    <button class="btn btn-sm btn-primary mb-2" content="Encaminhar/historico" v-tippy
                            v-show="!colaborador.resultado_integrado"
                            @click.prevent="formEncaminhar(colaborador)"
                            data-toggle="modal" data-target="#janelaParecerEntrevista">
                        <i class="fa fa-search-plus"></i> Abrir
                    </button>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <controle-paginacao class="d-flex justify-content-center" id="controle" ref="componente"
                        :url="urlPaginacao"
                        :por-pagina="controle.dados.porPagina" :dados="controle.dados" @carregou="carregou"
                        @carregando="carregando">
    </controle-paginacao>
@stop
@push('js')
    <script src="{{ mix('js/g/controle-exames/app.js') }}"></script>
@endpush
