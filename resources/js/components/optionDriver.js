export default {
    animate: true,  // Animate while changing highlighted element
    opacity: 0.75,  // Background opacity (0 means only popovers and without overlay)
    padding: 10,    // Distance of element from around the edges
    allowClose: false,
    doneBtnText: 'Finalizar', // Text on the final button
    closeBtnText: 'Fechar', // Text on the close button for this step
    nextBtnText: 'Próximo', // Next button text for this step
    prevBtnText: 'Anterior', // Previous button text for this step
    showButtons: true, // Do not show control buttons in footer
    keyboardControl: true, // Allow controlling through keyboard (escape to close, arrow keys to move)
}
