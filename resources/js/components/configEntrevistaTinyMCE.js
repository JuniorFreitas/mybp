export default {
    toolbar: ['underline'],
    menubar: false,
    statusbar: true,
    schema: 'html5',
    height: 650,
    resize: true,
    language: 'pt_BR',
    language_url: `${URL_SITE}/js/tinymce/langs/pt_BR.js`,
    branding: false,
    fontsize_formats: "12pt 14pt 18pt 24pt 36pt",
    plugins: "paste",
    paste_auto_cleanup_on_paste: true,
    paste_remove_styles: true,
    paste_remove_styles_if_webkit: true,
    paste_strip_class_attributes: true,
    content_style: 'body { font-size: 12pt; font-family: Arial; }',
    setup: function (ed) {
        ed.on('init', function (e) {
            ed.execCommand("fontName", false, "Arial");
            ed.execCommand("fontSize", false, "12pt");
        });
    },
    key: 'n166eatov70tbex79f7fll7p0fmngp7n7dosnk8rhxn5cdng',
}
