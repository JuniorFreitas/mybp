<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'disco-cloud' => [
            'driver' => 'local',
            'root' => storage_path('app/g/arquivos/disco-cloud'),
//            'url' => env('APP_URL').'/g/cloud',
            'url' => env('APP_URL') . '/publico/cloud',
        ],

        'disco-cliente' => [
            'driver' => 'local',
            'root' => storage_path('app/g/arquivos/disco-cliente'),
//            'url' => env('APP_URL') . '/g/administracao/clientes/',
            'url' => env('APP_URL') . '/g/storage/anexo/',
        ],

        'disco-fornecedor' => [
            'driver' => 'local',
            'root' => storage_path('app/g/arquivos/disco-fornecedor'),
            'url' => env('APP_URL') . '/g/administracao/fornecedor/',
        ],

        'disco-servicofornecedor' => [
            'driver' => 'local',
            'root' => storage_path('app/g/arquivos/disco-servicofornecedor'),
            'url' => env('APP_URL') . '/g/fornecedor/servico/',
        ],

        'disco-ocorrencia' => [
            'driver' => 'local',
            'root' => storage_path('app/g/arquivos/disco-ocorrencia'),
            'url' => env('APP_URL') . '/g/ocorrencia',
        ],

        'disco-fotocurriculo' => [
            'driver' => 'local',
            'root' => storage_path('app/g/arquivos/disco-fotocurriculo'),
            'url' => env('APP_URL') . '/g/admissao/',
        ],

        'documentos-funcionarios' => [
            'driver' => 'local',
            'root' => storage_path('app/g/arquivos/documentos-funcionarios'),
            'url' => env('APP_URL') . '/g/funcionarios',
        ],

        'evidencia-medidas' => [
            'driver' => 'local',
            'root' => storage_path('app/g/arquivos/medidas-evidencia'),
            'url' => env('APP_URL') . '/g/historico/medidas-administrativas',
        ],

        'evidencia-cih' => [
            'driver' => 'local',
            'root' => storage_path('app/g/arquivos/cih-evidencia'),
            'url' => env('APP_URL') . '/g/apontamento/cih',
        ],

        'disco-documentospreadmissao' => [
            'driver' => 'local',
            'root' => storage_path('app/g/arquivos/disco-documentospreadmissao'),
            'url' => env('APP_URL') . '/documentos',

        ],
        'disco-dossie' => [
            'driver' => 'local',
            'root' => storage_path('app/g/arquivos/disco-dossie'),
            'url' => env('APP_URL') . '/g/historico/dossie',

        ],
        /*'disco-ponto-eletronico' => [
            'driver' => 'local',
            'root' => storage_path('app/g/arquivos/disco-ponto-eletronico'),
            'url' => env('APP_URL') . '/g/controle-ponto/ponto-eletronico/fotos',
        ],*/
        'disco-ponto-eletronico' => [
            'driver' => env('FILESYSTEM_DRIVER', 'local'),
            'root' => env('FILESYSTEM_DRIVER')=='local' ? storage_path('app/g/arquivos/disco-ponto-eletronico'):'disco-ponto-eletronico',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
            'url' => env('FILESYSTEM_DRIVER')=='local' ? env('APP_URL') . '/g/controle-ponto/ponto-eletronico/fotos':env('AWS_URL'),
            'endpoint' => env('AWS_ENDPOINT'),
            'visibility' => 'public',
            /*'urlShow' => env('APP_URL').'/g/storage/anexo',
            'urlDownload' => env('APP_URL').'/g/storage/anexo',
            'urlThumb' => env('APP_URL').'/g/storage/anexo',
            'urlDelete' => env('APP_URL').'/g/storage/anexo',*/

        ],
        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        's3' => [
            //'driver' => 's3',
            'driver' => env('FILESYSTEM_DRIVER', 'local'),
            'root' => storage_path('app/g'),
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
            'url' => env('FILESYSTEM_DRIVER')=='local' ? env('APP_URL') . '/g':env('AWS_URL'),
            'endpoint' => env('AWS_ENDPOINT'),

            'urlShow' => env('APP_URL').'/g/storage/anexo',
            'urlDownload' => env('APP_URL').'/g/storage/anexo',
            'urlThumb' => env('APP_URL').'/g/storage/anexo',
            'urlDelete' => env('APP_URL').'/g/storage/anexo',
            'visibility' => 'public',
        ],



    ],

    /*
    |--------------------------------------------------------------------------
    | Symbolic Links
    |--------------------------------------------------------------------------
    |
    | Here you may configure the symbolic links that will be created when the
    | `storage:link` Artisan command is executed. The array keys should be
    | the locations of the links and the values should be their targets.
    |
    */

    'links' => [
        public_path('storage') => storage_path('app/public'),
    ],

];
